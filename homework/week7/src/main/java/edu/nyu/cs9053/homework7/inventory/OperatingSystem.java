package edu.nyu.cs9053.homework7.inventory;

/**
 * Created by jizhou on 3/28/16.
 */
public class OperatingSystem <T extends Electronic> implements Item {
    private final Double price;
    private final T elecronic;
    private final String osName;

    public OperatingSystem(double price, T elecronic, String osName){
        this.price = price;
        this.elecronic = elecronic;
        this.osName = osName;
    }

    @Override
    public Double getPrice(){
        return price;
    }

    public T getElecronic(){
        return elecronic;
    }

    public String getOsName(){
        return osName;
    }
}
