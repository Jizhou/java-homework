package edu.nyu.cs9053.homework7.inventory;

/**
 * Created by jizhou on 3/28/16.
 */
public class SoftwareDriver <T extends Electronic> implements Item {

    private final Double price;
    private final T electronic;
    private final String driverName;

    public SoftwareDriver(double price, T electronic, String driverName){
        this.price = price;
        this.electronic = electronic;
        this.driverName = driverName;
    }

    @Override
    public Double getPrice() {
        return price;
    }

    public T getElectronic(){
        return electronic;
    }

    public String getDriverName(){
        return driverName;
    }

}
