package edu.nyu.cs9053.homework7.inventory;

public class Computer extends Electronic{

    private final Double ghz;

    public Computer(double ghz, double price){
        super(price);
        this.ghz = ghz;
    }

    public Double getGhz(){
        return ghz;
    }
}