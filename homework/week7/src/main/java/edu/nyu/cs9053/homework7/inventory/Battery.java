package edu.nyu.cs9053.homework7.inventory;

/**
 * Created by jizhou on 3/28/16.
 */
public class Battery <T extends Electronic> implements Item{

    private final Double price;
    private final T electronic;
    private final double durationInHours;

    public Battery(double price, T electronic, double durationInHours){
        this.price = price;
        this.electronic = electronic;
        this.durationInHours = durationInHours;
    }

    @Override
    public Double getPrice(){
        return price;
    }

    public T getElectronic(){
        return electronic;
    }

    public double getDurationInHours(){
        return durationInHours;
    }
}
