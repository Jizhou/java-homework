package edu.nyu.cs9053.homework7.inventory;

/**
 * Created by jizhou on 3/28/16.
 */
public class Fiction <T extends Bookcover> implements Item{

    private final String title;
    private final Double price;
    private final T cover;

    public Fiction(String title, T cover){
        this.title = title;
        this.price = cover.getPrice()+cover.getBook().getPrice();
        this.cover = cover;
    }

    @Override
    public Double getPrice(){
        return price;
    }

    public String getTitle(){
        return title;
    }

    public T getCover() {
        return cover;
    }
}
