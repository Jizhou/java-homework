package edu.nyu.cs9053.homework7.inventory;

public class Softcover extends Book {

    public Softcover(String title, double price){
        super(title, price);
    }
}
