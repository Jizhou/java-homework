package edu.nyu.cs9053.homework7.inventory;

/**
 * Created by jizhou on 3/28/16.
 */
public class Dictionary <T extends Bookcover> implements Item{

    private final Double price;
    private final T bookcover;
    private final String language;

    public Dictionary(T bookcover, String language){
        this.price = bookcover.getPrice()+bookcover.getBook().getPrice();
        this.bookcover = bookcover;
        this.language = language;
    }

    @Override
    public Double getPrice(){
        return price;
    }

    public T getBookcover(){
        return bookcover;
    }

    public String getLanguage(){
        return language;
    }

}
