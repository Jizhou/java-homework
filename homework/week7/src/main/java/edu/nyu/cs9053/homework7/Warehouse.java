package edu.nyu.cs9053.homework7;
import edu.nyu.cs9053.homework7.inventory.*;

/**
 * User: blangel
 * Date: 10/13/14
 * Time: 1:57 PM
 */
public class Warehouse {

    private static<T> void copy(Bin<? extends T> from, Bin<? super T> into) {
	// TODO - copy values in 'from' to 'into'
        for(T obj : from){
            into.add(obj);
        }
    }

    // TODO - implement such that the warehouse can hold any Bin of Item type
    private final Bin<? extends Item> items;

    public Warehouse(Bin<? extends Item> items) {
        this.items = items;
    }

    public Bin<? extends Item> getItems() {
        return items;
    }

    public Warehouse copy() {
        // TODO - make a new Bin copying the values from `items` into a new Bin using the 'copy' method below
        // TODO - change to return a copied bin
        Bin copiedBin = new Bin<>();
        copy(items,copiedBin);
        return new Warehouse(copiedBin);
    }

}
