package edu.nyu.cs9053.homework7;
import edu.nyu.cs9053.homework7.inventory.*;
/**
 * User: blangel
 * Date: 10/13/14
 * Time: 1:58 PM
 */
public class WarehouseInventoryAuditor {

    // TODO - create a method to print the individual prices of a Bin of any Item type
    public static void printAny(Bin<? extends Item> bin){
        System.out.println("printing any item in bin");
        for(Item item: bin){
            System.out.println(item.getPrice());
        }
    }

    // TODO - create a method to print the individual prices of a Bin of any Electronic types
    public static void printElectronic(Bin<? extends Electronic> bin){
        System.out.println("printing electronic bin");
        for(Electronic electronic: bin){
            System.out.println(electronic.getPrice());
        }
    }

    // TODO - create a method to print the individual prices of a Bin of any Book types
    public static void printBook(Bin<? extends Book> bin){
        System.out.println("printing book bin");
        for(Book book: bin){
            System.out.println(book.getPrice());
        }
    }

}
