package edu.nyu.cs9053.homework7.inventory;

public class DigitalCamera extends Camera{

    private final Integer zoom;

    public DigitalCamera(int zoom, double price){
        super(price);
        this.zoom = zoom;
    }

    public int getZoom(){
        return Integer.valueOf(zoom);
    }
}