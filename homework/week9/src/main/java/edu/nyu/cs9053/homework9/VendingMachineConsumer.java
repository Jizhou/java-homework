package edu.nyu.cs9053.homework9;

import java.util.concurrent.Semaphore;

/**
 * Created by jizhou on 4/10/16.
 */
public class VendingMachineConsumer implements Consumer{

    private final Semaphore mulExcluSemaphore;

    public VendingMachineConsumer(Semaphore sema){
        mulExcluSemaphore = sema;
    }

    @Override
    public Seltzer consume(VendingMachine from){
        try{
            mulExcluSemaphore.acquire();
            if(!from.isEmpty()){
                Seltzer getSeltzer = from.remove();
                return getSeltzer;
            }else{
                return null;
            }
        }catch(InterruptedException ie){
            Thread.currentThread().interrupt();
            throw new RuntimeException();
        }finally {
            mulExcluSemaphore.release();
        }
    }
}
