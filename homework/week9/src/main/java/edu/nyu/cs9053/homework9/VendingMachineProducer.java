package edu.nyu.cs9053.homework9;

import java.util.Random;
import java.util.concurrent.Semaphore;

/**
 * Created by jizhou on 4/10/16.
 */
public class VendingMachineProducer implements Producer{

    private static final Random RANDOM = new Random();

    private final Semaphore mulExcluSemaphore;

    public VendingMachineProducer(Semaphore sema){
        mulExcluSemaphore = sema;
    }

    @Override
    public Seltzer produce(VendingMachine into){
        try {
            int pick = RANDOM.nextInt(Flavor.values().length);
            Seltzer newSeltzer = new Seltzer(Flavor.values()[pick]);
            mulExcluSemaphore.acquire();
            if (!into.atCapacity()) {
                into.add(newSeltzer);
                return newSeltzer;
            }else{
                return null;
            }
        }catch(InterruptedException ie){
            Thread.currentThread().interrupt();
            throw new RuntimeException();
        }
        finally {
            mulExcluSemaphore.release();
        }
    }
}
