package edu.nyu.cs9053.homework8;

import java.util.Collections;
import java.util.List;

/**
 * User: blangel
 * Date: 10/14/14
 * Time: 6:47 PM
 */
public class Lists {

    public static <T> List<T> shuffle(List<T> list) {
	// TODO
        Collections.shuffle(list);
        return list;
    }

    public static <T> boolean deepEquals(List<T> left, List<T> right) {
    // TODO
        if(left == null || right == null) return false;
        if(left.size() != right.size()) return false;
        boolean result = true;
        for(int i = 0; i < left.size(); i++){
            if(!left.get(i).equals(right.get(i))){
                result = false;
                break;
            }
        }
        return result;
    }

}
