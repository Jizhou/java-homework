package edu.nyu.cs9053.homework8;

import java.util.*;

/**
 * Created by jizhou on 4/4/16.
 */
public abstract class AbstractMultiMap <K, V> implements Multimap<K,V>{

    private final Map<K, Collection<V>> delegate;

    public AbstractMultiMap(Map<K, Collection<V>> delegate){
        this.delegate = delegate;
    }

    public Map<K, Collection<V>> getDelegate(){
        return delegate;
    }

    @Override
    public Collection<V> put(K key, Collection<V> value){
        return delegate.put(key,value);
    }

    @Override
    public int size(){
        return delegate.size();
    }

    @Override
    public boolean isEmpty(){
        return delegate.isEmpty();
    }

    @Override
    public boolean containsKey(Object v1){
        return delegate.containsKey(v1);
    }

    @Override
    public boolean containsValue(Object v1){
        return delegate.containsValue(v1);
    }

    @Override
    public Collection<V> get(Object obj){
        return delegate.get(obj);
    }

    @Override
    public Collection<V> remove(Object obj) {
        return delegate.remove(obj);
    }

    @Override
    public void putAll(Map<? extends K, ? extends Collection<V>> map){
        delegate.putAll(map);
    }

    @Override
    public void clear(){
        delegate.clear();
    }

    @Override
    public Set<K> keySet(){
        return delegate.keySet();
    }

    @Override
    public Collection<Collection<V>> values(){
        return delegate.values();
    }

    @Override
    public Set<Map.Entry<K , Collection<V>>> entrySet(){
        return delegate.entrySet();
    }

    @Override
    public boolean equals(Object obj){
        return delegate.equals(obj);
    }

    @Override
    public int hashCode(){
        return delegate.hashCode();
    }

}
