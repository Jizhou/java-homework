package edu.nyu.cs9053.homework8;

import java.util.*;

/**
 * Created by jizhou on 3/31/16.
 */
public class MultiHashMap <K,V> extends AbstractMultiMap <K,V>{

    private final Map<K, Collection<V>> delegate;

    public MultiHashMap(){
        super(new HashMap<K, Collection<V>>());
        delegate = super.getDelegate();
    }


    @Override
    public Collection<V> put(K key, Collection<V> value){
        if(value instanceof LinkedList){
            return delegate.put(key,value);
        }
        return null;
    }

    @Override
    public boolean putItem(K key, V value){
        if(key == null || value == null) return false;
        Collection<V> keyCollection = delegate.get(key);
        if(keyCollection == null){
            LinkedList<V> valueList = new LinkedList<>();
            valueList.add(value);
            delegate.put(key,valueList);
        }else{
            keyCollection.add(value);
        }
        return true;
    }

    @Override
    public Collection<V> getItems(K key){
        return get(key);
    }

}
