package edu.nyu.cs9053.homework8;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * User: blangel
 * Date: 10/14/14
 * Time: 6:47 PM
 */
public class Sets {

    public static <T> Set<T> union(Set<T> left, Set<T> right) {
	// TODO
        Set<T> unionSet = new HashSet<>();
        for(T leftElem : left) {
            unionSet.add(leftElem);
        }
        for(T rightElem : right){
            unionSet.add(rightElem);
        }
        return unionSet;
    }

    public static <T> Set<T> intersection(Set<T> left, Set<T> right) {
	// TODO
        if(left == null || right == null) return null;
        Set<T> interSet = new HashSet<>();
        for(T leftElem : left){
            if(right.contains(leftElem)){
                interSet.add(leftElem);
            }
        }
        return interSet;
    }

    public static <T> Set<T> symmetricDifference(Set<T> left, Set<T> right) {
	// TODO
        Set<T> interSet = intersection(left,right);
        Set<T> unionSet = union(left,right);
        for(T element : interSet) {
            unionSet.remove(element);
        }
        return unionSet;
    }

}
