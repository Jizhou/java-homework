package edu.nyu.cs9053.homework8;

import java.util.*;

/**
 * Created by jizhou on 3/31/16.
 */
public class MultiTreeMap<K, V> extends AbstractMultiMap<K, V>{

    private final Map<K, Collection<V>> delegate;

    public MultiTreeMap(){
        super(new TreeMap<K,Collection<V>>());
        this.delegate = super.getDelegate();
    }

    @Override
    public Collection<V> put(K key, Collection<V> value){
        if(value instanceof ArrayList) {
            return delegate.put(key, value);
        }
        return null;
    }

    @Override
    public boolean putItem(K key, V value) {
        if (key == null || value == null) return false;
        Collection<V> keyValue = delegate.get(key);
        if (keyValue == null) {
            ArrayList<V> valueList = new ArrayList<>();
            valueList.add(value);
            delegate.put(key, valueList);
        } else {
            keyValue.add(value);
        }
        return true;
    }

    @Override
    public Collection<V> getItems(K key){
        return get(key);
    }

}
