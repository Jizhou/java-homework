package edu.nyu.cs9053.homework2;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * User: blangel
 * Date: 9/5/15
 * Time: 10:24 AM
 *
 * Hint, to compute the future value of an annuity
 * FVa = P * [ (((1 + r)^t) - 1) / r ]
 *  where P is the payment amount
 *  where r is the interest rate
 *  and where t is the time in years (e.g., 6 months t=0.5)
 *
 * Hint, to compute the future value of an annuity with compounding
 * FVac = P * [ (((1 + (r / m))^(m * t)) - 1) / (r / m) ]
 *  where P is the payment amount
 *  where r is the interest rate
 *  where m is the number of compounding periods in a year (e.g., annually m=1, semiannually m=2, quarterly m=4, monthly m=12)
 *  and where t is the time in years (e.g., 6 months t=0.5)
 */
public class AnnuityCalculator {

    /**
     * Use this scale when doing BigDecimal division.
     */
    private static final int DEFAULT_SCALE = 10;

    /**
     * Use this rounding mode when doing BigDecimal division.
     */
    private static final RoundingMode DEFAULT_ROUNDING_MODE = RoundingMode.HALF_UP;

    private BigDecimal computeAnnuityFormula(BigDecimal payAmount, BigDecimal intRate, int years){
	/**
	* this method reads in BigDecimal type pay amount, interest rate and integer type years
	* process the calculation
	* return: BidDecimal type calculated annuity.
	**/
	BigDecimal result = intRate.add(new BigDecimal("1")).pow(years).subtract(new BigDecimal("1")).multiply(payAmount).divide(intRate,DEFAULT_SCALE,RoundingMode.HALF_UP);
	result = result.setScale(5,RoundingMode.HALF_DOWN);
	result = result.setScale(DEFAULT_SCALE,RoundingMode.HALF_DOWN);
	System.out.println(result);
	return result;
    }


    public BigDecimal computeFutureValueOfAnnuityIn15Years(double annuityAmount, double annualInterestRateInPercent) {
	return computeFutureValueOfAnnuity(annuityAmount, annualInterestRateInPercent,15);
    }

    public BigDecimal computeFutureValueOfAnnuityIn30Years(double annuityAmount, double annualInterestRateInPercent) {
    	return computeFutureValueOfAnnuity(annuityAmount,annualInterestRateInPercent,30);
    }

    public BigDecimal computeMonthlyCompoundedFutureValueOfAnnuityIn15Years(double annuityAmount, double annualInterestRateInPercent) {
    	return computeMonthlyCompoundedFutureValueOfAnnuity(annuityAmount, annualInterestRateInPercent,15);
    }

    public BigDecimal computeMonthlyCompoundedFutureValueOfAnnuityIn30Years(double annuityAmount, double annualInterestRateInPercent) {
    	return computeMonthlyCompoundedFutureValueOfAnnuity(annuityAmount, annualInterestRateInPercent,30);
    }

    public BigDecimal computeFutureValueOfAnnuity(double annuityAmount, double annualInterestRateInPercent, int years) {
	String annuityAmountStr = Double.toString(annuityAmount);
	String inRateStr = Double.toString(annualInterestRateInPercent);
	BigDecimal annuityAmountBig = new BigDecimal(annuityAmountStr);
	BigDecimal inRateBig = new BigDecimal(inRateStr);
	inRateBig = inRateBig.divide(new BigDecimal("100"));
	return computeAnnuityFormula(annuityAmountBig,inRateBig,years);
	
    }

    public BigDecimal computeMonthlyCompoundedFutureValueOfAnnuity(double annuityAmount, double annualInterestRateInPercent, int years) {
	String annuityAmountStr = Double.toString(annuityAmount);
	String inRateStr = Double.toString(annualInterestRateInPercent);
	BigDecimal annuityAmountBig = new BigDecimal(annuityAmountStr);
	BigDecimal inRateBig = new BigDecimal(inRateStr);
	inRateBig = inRateBig.divide(new BigDecimal("100.0")).divide(new BigDecimal(12),DEFAULT_SCALE,RoundingMode.HALF_UP);
	return computeAnnuityFormula(annuityAmountBig,inRateBig,years*12);

    }

    public static void main(String[] args){
	AnnuityCalculator annuityCal = new AnnuityCalculator();
	BigDecimal result1 = annuityCal.computeFutureValueOfAnnuity(400000,3.5,15);
    	BigDecimal result2 = annuityCal.computeMonthlyCompoundedFutureValueOfAnnuity(400000,3.5,15);
    }

}
