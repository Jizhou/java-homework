package edu.nyu.cs9053.homework2;

/**
 * User: blangel
 * Date: 8/17/14
 * Time: 10:21 AM
 */
public class Program {

    /**
    * In order to check command-line argument and determine which operation to process
    * define 4 types of mode including command-line argument is incorrect which is INVALID_ARGS
    * doing GPS calculation, which is GPS_MODE
    * doing annuity calculation which is ANNUITY_MODE
    * doing annuity compound calculation which is ANNUITY_COMPOUND_MODE
    **/

    private static final byte INVALID_ARGS = 0;
    private static final byte GPS_MODE = 1;
    private static final byte ANNUITY_MODE = 2;
    private static final byte ANNUITY_COMPOUND_MODE = 3;   

    private static byte argsCheck(String[] args){
	/**
	* read in command-line argument 
	* return mode number
	**/

	byte mode = INVALID_ARGS;

	if(args.length==0) return mode;

	if(args[0].compareTo("gps")!=0 && args[0].compareTo("annuity")!=0 ) mode = INVALID_ARGS;
	else if(args[0].compareTo("annuity")==0){
		if(args[1].compareTo("compound")==0 && args.length==5){
			mode = ANNUITY_COMPOUND_MODE;
		}else if(args.length==4){
			mode = ANNUITY_MODE;
		}else mode = INVALID_ARGS;
	}else if(args[0].compareTo("gps")==0) {
		mode = GPS_MODE;
	}
	else mode = INVALID_ARGS;

	return mode;
    }

    public static void main(String[] args) {
	/**
	* for mode in ANNUITY_MODE, ANNUITY_COMPOUND_MODE, GPS_MODE,
    	* first try to convert the argument into required data type
	* if cannot, turn the mode into INVALID_ARGS	
	**/

	int count = args.length;
	byte mode = argsCheck(args);
	if(mode==ANNUITY_MODE){
		int years=0;
		double annuityAmount=0d;
		double annualInterestRateInPercent=0d;
		AnnuityCalculator annualCal = new AnnuityCalculator();
		try{
			years = Integer.parseInt(args[3]);
			annuityAmount = Double.parseDouble(args[1]);
			annualInterestRateInPercent = Double.parseDouble(args[2]);
			
		}catch(NumberFormatException e){
			mode = INVALID_ARGS;
		}
		if(mode!=INVALID_ARGS){
			if(years==15){
				annualCal.computeFutureValueOfAnnuityIn15Years(annuityAmount,annualInterestRateInPercent);
			}else if(years==30){
				annualCal.computeFutureValueOfAnnuityIn30Years(annuityAmount,annualInterestRateInPercent);
			}else{
				annualCal.computeFutureValueOfAnnuity(annuityAmount,annualInterestRateInPercent,years);
			}
		}
	}else if(mode==ANNUITY_COMPOUND_MODE){
		AnnuityCalculator annualCal = new AnnuityCalculator();
		int years=0;
		double annuityAmount=0d;
		double annualInterestRateInPercent=0d;
		try{
			years = Integer.parseInt(args[4]);
			annuityAmount = Double.parseDouble(args[2]);
			annualInterestRateInPercent = Double.parseDouble(args[3]);
		}catch(NumberFormatException e){
			mode = INVALID_ARGS;
		}
		//System.out.println("mode:"+mode);
		if(mode!=INVALID_ARGS){
			if(years==15){
				annualCal.computeMonthlyCompoundedFutureValueOfAnnuityIn15Years(annuityAmount,annualInterestRateInPercent);
			}else if(years==30){
				annualCal.computeMonthlyCompoundedFutureValueOfAnnuityIn30Years(annuityAmount,annualInterestRateInPercent);
			}else{
				annualCal.computeMonthlyCompoundedFutureValueOfAnnuity(annuityAmount,annualInterestRateInPercent,years);
			}
		}
	}else if(mode==GPS_MODE){
		count--;
		PolylineEncoder polyEncoder = new PolylineEncoder();
		double latitude=0d;
		double longitude=0d;
		String[] parts = new String[2];
		String resultStr = "";
		Gps[] gpsArray = new Gps[count];
		if(count==0){
			mode=INVALID_ARGS;
		}else{
			try{
				for(int i=0; i<count; i++){
					parts = args[i+1].split(",");
					latitude = Double.parseDouble(parts[0]);
					longitude = Double.parseDouble(parts[1]);
					gpsArray[i] = new Gps(latitude,longitude);
				}
			}catch(NumberFormatException e){
				mode = INVALID_ARGS;
			}
		}


		if(mode!=INVALID_ARGS){
			resultStr = polyEncoder.encodePolyline(gpsArray);
			System.out.println(resultStr);
		}
	}
	
	if(mode==INVALID_ARGS){
		System.out.println("Invalid argument");
	}

    }

}
