package edu.nyu.cs9053.homework2;

/**
 * User: blangel
 * Date: 8/17/14
 * Time: 9:02 AM
 *
 * Implements the Polyline Algorithm defined here
 * {@literal https://developers.google.com/maps/documentation/utilities/polylinealgorithm}
 */
public class PolylineEncoder {

    private int binaryOperations(int num){
	/**
	* This method do first, signed left move 1
	* second, inverse the num if it is smaller than 0
	* return: int value
	**/
	num = num << 1;
	if(num<0) num = ~num;
	//System.out.println("after left move 1:"+num);
	return num;
    }

    private byte[] chunksOperations(int num){
	/**
 	* According to Polyline google code
	* first, divide num into 5 32-bit chunks
	* then doing OR operation and ADD operation
	* return: a byte array.
	**/
	byte[] results = new byte[10];
	int len=0;
	byte tempRemainder = 0;

	while(true){
		tempRemainder = (byte)(num%32);
		results[len] = (byte)tempRemainder;
		num /= 32;
		len++;
		if(num==0) break;
	}

	for(int i=0; i<len; i++){
		if(i!=(len-1)){
			results[i] = (byte)((results[i]|0x20)+63);
		}else results[i] = (byte)(results[i]+63);
		//System.out.println(i+"is"+results[i]);	
	}
	return results;
    }

    public String encodePolyline(Gps[] gpsPoints) {
	/**
	* this method read in a Gps array and convert it into corresponding String
	* Return: a string
	**/
	int len = gpsPoints.length;
	String str = new String();
	str = "";
	char tempChar = 0;
	for(int i=0; i<len; i++){
		int latitude = (int)(gpsPoints[i].getLatitude() * 1e5);
		int longitude = (int)(gpsPoints[i].getLongitude() * 1e5);
		if(i >= 1){
			latitude = latitude-(int)(gpsPoints[i-1].getLatitude() * 1e5);
			longitude = longitude-(int)(gpsPoints[i-1].getLongitude() * 1e5);
		}
		//System.out.println("latitude:"+latitude);
		//System.out.println("longitude:"+longitude);
		latitude = binaryOperations(latitude);
		longitude = binaryOperations(longitude);
		byte[] latiBytes = chunksOperations(latitude);
		byte[] longBytes = chunksOperations(longitude);
		for(int j=0; latiBytes[j]!=0; j++){
			str+=(char)latiBytes[j];
		}
		for(int j=0; longBytes[j]!=0; j++){
			str+=(char)longBytes[j];
		}
	}
	return str;
    }
    public static void main(String[] args){
	PolylineEncoder pe = new PolylineEncoder();
	Gps[] gpsArray = new Gps[3];
	gpsArray[0] = new Gps(38.5,-120.2);
	gpsArray[1] = new Gps(40.7,-120.95);
	gpsArray[2] = new Gps(43.252,-126.453);
	String str = pe.encodePolyline(gpsArray);
	System.out.println(str);
    }
}
