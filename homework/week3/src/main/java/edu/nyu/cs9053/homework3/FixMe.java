package edu.nyu.cs9053.homework3;

/**
 * User: blangel
 * Date: 8/23/14
 * Time: 11:45 AM
 */
import edu.nyu.cs9053.homework3.metadata.FixMeToo;

public class FixMe {

    private final String name;

    private final String secondary;

    public FixMe(String name) {
	this(name,"");
    }

    public FixMe(String name, String secondary) {
        this.name = name;
	this.secondary = new FixMeToo(!"".equals(secondary)).analyzeMetadata(secondary);
    }

    public FixMe changeName(String name) {
        return new FixMe(name,this.secondary);
    }

    public FixMe changeName(String firstName, String lastName, String secondary) {
        return new FixMe(changeName(firstName, lastName), secondary);
    }

    public String changeName(String firstName, String lastName) {
        return String.format("%s %s", firstName, lastName);
    }

}
