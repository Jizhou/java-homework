package edu.nyu.cs9053.homework3.dance;

public class DanceMove{

    private final String idealMove;
    private final String userMove;

    public DanceMove(String userMove, String idealMove){
	this.idealMove = idealMove;
	this.userMove = userMove;
    }

    public String getIdealMove(){
	return this.idealMove;
    }
    public String getUserMove(){
	return this.userMove;
    }
    public boolean correctMove(){
	return userMove.compareTo(idealMove)==0;
    }
}
