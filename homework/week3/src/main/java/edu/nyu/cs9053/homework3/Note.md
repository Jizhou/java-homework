##Note for HW3
* Use classes other than current package, must IMPORT
* Render all other constructor finally call one MAIN constructor
* Final variable must be initialized in the constructor
* array.length whereas string.length()
* static methods/variables load first at compiling phase, which is the reason why I could not use just private method to be called in constructor.
* In constructor, using this() could overload another constructor, but this() have to be the statement. We could write static method as parameter in this() method.
