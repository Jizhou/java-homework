package edu.nyu.cs9053.homework3.dance;

public class DanceCat{

    public static int getComputerLevel(){
	return 60;
    }

    private static String[] calParsedMoves(String unparsedMoves,int len){
	String[] parsedMoves = new String[len];
	for(int i=0; i < len; i++){
		if(i < unparsedMoves.length()){
			parsedMoves[i] = Character.toString(unparsedMoves.charAt(i));
		}else{
			parsedMoves[i] = "";
		}
	}
	return parsedMoves;
    }

    private static DanceMove[] calDanceMoves(String[] moves, String[] idealMoves){
	DanceMove[] danceMoves = new DanceMove[idealMoves.length];
	for(int i=0; i<idealMoves.length; i++){
		danceMoves[i] = new DanceMove(moves[i],idealMoves[i]);
	}
	return danceMoves;
    }

    private final String name;
    private final DanceMove[] danceMoves;

    public DanceCat(String unparsedMoves, String[] idealMoves){
	this(calParsedMoves(unparsedMoves,idealMoves.length),idealMoves);
    }
  
    public DanceCat(String[] moves, String[] idealMoves){
	this(calDanceMoves(moves,idealMoves));
    }

    public DanceCat(DanceMove[] danceMoves){
	this("Cat",danceMoves);
    }

    public DanceCat(String name, DanceMove[] danceMoves){
	this.name = name;
	this.danceMoves = danceMoves;
    }

    public String getName(){
	return this.name;
    }

    public DanceMove[] getDanceMoves(){
	return this.danceMoves;
    }

    public int getNumberOfCorrectMoves(){
	int numOfCorrect=0;
	for(int i=0; i<danceMoves.length; i++){
		if(danceMoves[i].correctMove()==true)
			numOfCorrect++;
	}
	return numOfCorrect;
    }


}
