package edu.nyu.cs9053.homework5;

/**
 * User: blangel
 * Date: 9/21/14
 * Time: 6:01 PM
 */
public class MadScientist {

    private static final double TRAVEL_STRENGTH = 100.0;
    private static final double YEARS_THRESHOLD = 0.001d;
    private static final int TRAVELER_NUM = 3;

    private final TimeMachine timeMachine;

    public MadScientist(TimeMachine timeMachine) {
        this.timeMachine = timeMachine;
    }

    public static void main(String[] args) {
        // make a MadScientist / TimeMachine and 3 TimeTraveler implementations
        // experiment on each TimeTraveler
        // a TimeTraveler should always start with 100 years of time travel strength
        // one TimeTraveler implementation should linearly decay (i.e., one year of actual time travel reduces the
        // time traveler's ability by one year)
        // one TimeTraveler implementation should decay double the travel value (i.e., one year of actual time travel reduces
        // the time traveler's ability by two years)
        // one TimeTraveler implementation should have exponential decay with a decay constant inputted by the scientist (see http://en.wikipedia.org/wiki/Exponential_decay)
	
        // continue to experiment until all the TimeTraveler's have exhausted their ability to travel
	
	MadScientist madScientist = new MadScientist(new TimeMachine());
	TimeTraveler[] timeTravelers = new TimeTraveler[TRAVELER_NUM];
 	timeTravelers[0] = new LinearTraveler("linear man", TRAVEL_STRENGTH);
        timeTravelers[1] = new DoubleTraveler("double man", TRAVEL_STRENGTH);
        timeTravelers[2] = new ExponentialTraveler("expo man", TRAVEL_STRENGTH);

	for(TimeTraveler oneTraveler: timeTravelers){
		madScientist.experiment(oneTraveler);
	}
    }

    public void experiment(TimeTraveler timeTraveler) {
        // invoke the time-machine and print how much time has passed using a callback and adjust the time traveler's ability to travel
	final TimeTraveler timeTravelerFinal = timeTraveler;

	this.timeMachine.travel(timeTraveler, new TimeTravelCallback(){

		@Override
		public void leaped(Time unit, int amount, boolean ahead){
			double remainYears = timeTravelerFinal.getRemainingYearsOfTravel();
			String name = timeTravelerFinal.getName();

			if(ahead){
				System.out.println(name+" is traveling to future!");
			}else{
				System.out.println(name+" is traveling back!");
			}

	 		if(remainYears >= YEARS_THRESHOLD){
				System.out.println(name+" has "+remainYears+" years left!");
				timeTravelerFinal.adjust(unit, amount, ahead);
				experiment(timeTravelerFinal);			
			}else{
				System.out.println(name+" has no more time ability to travel");
			}
		}
	});

    }

    public boolean stopRunExperiment(TimeTraveler timeTraveler){
	if(timeTraveler.getRemainingYearsOfTravel() < YEARS_THRESHOLD){
		return true;
	}
	return false;
    }

}
