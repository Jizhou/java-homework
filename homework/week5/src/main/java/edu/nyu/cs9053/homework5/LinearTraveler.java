package edu.nyu.cs9053.homework5;

public class LinearTraveler extends AbstractTraveler implements TimeTraveler{

    public LinearTraveler(String name, double remainingYears){
	super(name, remainingYears);
    }

    @Override
    public void adjust(Time unit, int amount, boolean ahead){
        double travelInYears = 0d;
	if(unit == Time.Hours){
		travelInYears = (double)amount/365.0/24.0;
	}else{
		travelInYears = (double)amount/365.0;
	}
	setRemainingYearsOfTravel(getRemainingYearsOfTravel()-travelInYears);
    }

}
