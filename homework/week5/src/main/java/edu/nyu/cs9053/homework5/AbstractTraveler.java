package edu.nyu.cs9053.homework5;

import java.util.concurrent.atomic.AtomicReference;

public abstract class AbstractTraveler{

    private final String name;
    private final AtomicReference<Double> remainingYears;

    public AbstractTraveler(String name, double remainingYears){
	this.name = name;
	this.remainingYears = new AtomicReference<Double>(remainingYears);
    }

    public String getName(){
	return this.name;
    }

    public Double getRemainingYearsOfTravel(){
	return new Double(this.remainingYears.get());
    }

    public void setRemainingYearsOfTravel(double newRemainingYears){
	remainingYears.set(newRemainingYears);
    }

}
