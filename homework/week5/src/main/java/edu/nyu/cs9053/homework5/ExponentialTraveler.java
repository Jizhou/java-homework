package edu.nyu.cs9053.homework5;

public class ExponentialTraveler extends AbstractTraveler implements TimeTraveler{

    private final static int EXPONENTIAL_RATE = 2;

    public ExponentialTraveler(String name, double remainingYears){
        super(name, remainingYears);
    }

    @Override
    public void adjust(Time unit, int amount, boolean ahead){
        double travelInYears = 0d;
	if(unit == Time.Hours){
		travelInYears = Math.exp(-(double)amount/EXPONENTIAL_RATE/365.0/24.0);
	}else{
		travelInYears = Math.exp(-(double)amount/EXPONENTIAL_RATE/365.0);
        }
	setRemainingYearsOfTravel(getRemainingYearsOfTravel()*travelInYears);
    }
}    
