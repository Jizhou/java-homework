package edu.nyu.cs9053.homework4.hierarchy;

public abstract class Cactus{
    
    private final String name;
    private final String location;
    private final LeafType leafType;

    protected Cactus(String name, String location, LeafType leafType){
	this.name = name;
	this.location = location;
	this.leafType = leafType;
    }

    public boolean isFlowering(){
	return true;
    }

    public abstract boolean hasEdibleFruit();

    public String getName(){
	return this.name;
    }
    public String getLocation(){
	return this.location;
    }
    public LeafType getLeafType(){
	return this.leafType;
    }

    @Override
    public boolean equals(Object o){		// Modifier 1: descriptive name
	if(this == o){
		return true;
	}
	if((o == null) || (getClass() != o.getClass())){
		return false;
	}
	Cactus that = (Cactus)o;

	if((name == null) && (that.name != null)){		// null cannot call equals()
		return false;
  	}
	if((name != null) && !name.equals(that.name)){
		return false;
	}
	if((location == null) && (that.location != null)){
		return false;
	}
	if((location != null) && !location.equals(that.location)){
		return false;
	}
	return((leafType == null)?(that.leafType == null):leafType.equals(that.leafType));
    }

    @Override
    public int hashCode(){			// Modifer 2: getClass() hashCode
	int result = name!=null? name.hashCode():0;
	result = 31*result+(location!=null? location.hashCode():0);
	result = 31*result+(leafType!=null? leafType.hashCode():0);
	return result;
    }

}
