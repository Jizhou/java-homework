package edu.nyu.cs9053.homework4.hierarchy;

public enum Desert{

    ANTARCTIC(14.2*1e6),
    ARCTIC(13.9*1e6),
    SAHARA(9.1*1e6),
    ARABIAN(2.6*1e6),
    GOBI(1.3*1e6),
    PATAGONIAN(670*1e3),
    GREAT_VICTORIA(647*1e3),
    KALAHARI(570*1e3),
    GREAT_BASIN(490*1e3),
    SYRIAN(490*1e3);

    public static void printDeserts(Desert ... deserts){
	for(Desert eachDesert: deserts){
		System.out.println(eachDesert.getAreaKm());
        }
    }

    private final double areaKm;

    private Desert(double areaKm){
	this.areaKm = areaKm;
    }

    public double getAreaKm(){
	return areaKm;
    }
}
