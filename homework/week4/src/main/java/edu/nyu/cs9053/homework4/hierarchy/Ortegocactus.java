package edu.nyu.cs9053.homework4.hierarchy;

public class Ortegocactus extends Cactoideae{

    private final String flowerShape;

    public Ortegocactus(String name, String location){
	this(name, location, "circle");
    }

    public Ortegocactus(String name, String location, String flowerShape){
	super(name, location, LeafType.SPINE);
	this.flowerShape = flowerShape;
    }

    public String getFlowerShape(){
	return this.flowerShape;
    }

    @Override
    public boolean equals(Object thatObject){
	boolean equal = super.equals(thatObject);
	if(equal){
		Ortegocactus that = (Ortegocactus) thatObject;
		if(flowerShape == null && that.flowerShape != null){
			return false;
		}
		if(flowerShape != null && !flowerShape.equals(that.flowerShape)){
			return false;
		}
		return true;
	}
	return false;
    }

    @Override
    public int hashCode(){
	int result = super.hashCode();
	result = 31*result + (flowerShape !=null? flowerShape.hashCode(): 0);
	return result;
    }

}
