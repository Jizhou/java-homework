package edu.nyu.cs9053.homework4.hierarchy;

public abstract class Opuntioideae extends Cactus{
    
    protected Opuntioideae(String name, String location, LeafType leafType){
	super(name,location,leafType);
    } 

    public boolean hasEdibleFruit(){
	return true;
    }

}
