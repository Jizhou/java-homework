package edu.nyu.cs9053.homework4.hierarchy;

public class Consolea extends Opuntioideae{

    private final float price;

    public Consolea(String name, String location){
	this(name, location, 5.0f);
    }

    public Consolea(String name, String location, float price){
	super(name, location, LeafType.SPINE);
	this.price = price;
    }

    public float getPrice(){
	return this.price;
    }

    @Override
    public boolean equals(Object thatObject){
	boolean equal = super.equals(thatObject);
	if(equal){
		Consolea that = (Consolea) thatObject;
		if(price == that.price){
			return true;
		}
 	}
	return false;
    }

    @Override
    public int hashCode(){
	int result = super.hashCode();
	Float floatObj = new Float(price);
	result = 31*result + floatObj.hashCode();
	return result;
    }

}
