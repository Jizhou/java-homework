package edu.nyu.cs9053.homework4.hierarchy;

public abstract class Cactoideae extends Cactus{
    
    public Cactoideae(String name, String location, LeafType leafType){
	super(name, location, leafType);
    }

    public boolean hasEdibleFruit(){
	return false;
    }

}
