package edu.nyu.cs9053.homework4.hierarchy;

public class Armatocereus extends Cactoideae{

    private final float height;

    public Armatocereus(String name, String location){
	this(name, location, 75.0f);		// Centimeters
    }

    public Armatocereus(String name, String location, float height){
	super(name, location, LeafType.SPINE);
	this.height = height;
    }

    public float getHeight(){
	return this.height;
    }

    @Override
    public boolean equals(Object thatObject){
	boolean equal = super.equals(thatObject);
	if(equal){
		Armatocereus that = (Armatocereus) thatObject;
		if(height == that.height){
			return true;
		}
     	}	
	return false;
    }

    @Override
    public int hashCode(){
	int result = super.hashCode();
	Float floatObj = new Float(height);
	result = result*31 + floatObj.hashCode();
	return result;
    }

}
