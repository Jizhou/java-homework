package edu.nyu.cs9053.homework4.hierarchy;

public abstract class Pereskioideae extends Cactus{

    protected Pereskioideae(String name, String location, LeafType leafType){
	super(name,location,leafType);
    }

    public boolean hasEdibleFruit(){
	return false;
    }

}
