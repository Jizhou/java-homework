package edu.nyu.cs9053.homework4.hierarchy;

public class Turbinicarpus extends Cactoideae{

    private final String flowerColor;

    public Turbinicarpus(String name, String location){
	this(name, location, "purple");
    }
    public Turbinicarpus(String name, String location, String flowerColor){
	super(name, location, LeafType.SPINE);
	this.flowerColor = flowerColor;
    }
    
    public String getFlowerColor(){
	return this.flowerColor;
    }

    @Override
    public boolean equals(Object thatObject){
	boolean equal = super.equals(thatObject);
 	if(equal){
		Turbinicarpus that = (Turbinicarpus) thatObject;
		if(flowerColor == null && that.flowerColor != null){
			return false;
		}
		if(flowerColor != null && !flowerColor.equals(that.flowerColor)){
			return false;
		}
		return true;
	}
	return false;
    }

    @Override
    public int hashCode(){
	int result = super.hashCode();
	result = 31*result + (flowerColor != null? flowerColor.hashCode():0);
	return result;
    }
}
