package edu.nyu.cs9053.homework4.hierarchy;

public class Corphantha extends Cactoideae{

    private final float spineLength;

    public Corphantha(String name, String location){
	this(name, location, 10.0f);	//Centimeters
    }

    public Corphantha(String name, String location, float spineLength){
	super(name, location, LeafType.SPINE);
	this.spineLength = spineLength;	
    }

    public float getSpineLength(){
	return this.spineLength;
    }

    @Override
    public boolean equals(Object thatObject){
    	boolean equal = super.equals(thatObject);
	if(equal){
		Corphantha that = (Corphantha) thatObject;
		if(spineLength == that.spineLength){
			return true;
		}
	}
	return false;
    }

    @Override
    public int hashCode(){
	int result = super.hashCode();
	Float floatObj = new Float(spineLength);
	result = 31*result + floatObj.hashCode();
	return result;
    }

}
