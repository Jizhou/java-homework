package edu.nyu.cs9053.homework4.hierarchy;

public class Opuntia extends Opuntioideae{

    private final float weightInPound;

    public Opuntia(String name, String location){
	this(name, location, 2.0f);
    }
    public Opuntia(String name, String location, float weight){
	super(name, location, LeafType.LEAF);
	this.weightInPound = weight;
    }

    public float getWeightInPound(){
	return this.weightInPound;
    }

    @Override
    public boolean equals(Object thatObject){
	boolean equal = super.equals(thatObject);
	if(equal){
		Opuntia that = (Opuntia) thatObject;
		if(weightInPound == that.weightInPound){
			return true;
		}
	}
	return false;
    }

    @Override
    public int hashCode(){
	int result = super.hashCode();
	Float floatObj = new Float(weightInPound);
	result = 31*result+floatObj.hashCode();
	return result;
    }
    
}
