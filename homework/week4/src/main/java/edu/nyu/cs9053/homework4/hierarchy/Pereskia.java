package edu.nyu.cs9053.homework4.hierarchy;

public class Pereskia extends Pereskioideae{

    private final float waterPercentage;

    public Pereskia(String name, String location){
	this(name, location, 0.25f);
    }

    public Pereskia(String name, String location, float waterPct){
	super(name, location, LeafType.LEAF);
	this.waterPercentage = waterPct;
    }

    public float getWaterPercentage(){
	return this.waterPercentage;
    }

    @Override
    public boolean equals(Object thatObject){
	boolean equal = super.equals(thatObject);
	if(equal){
		Pereskia that = (Pereskia) thatObject;
		if(waterPercentage == that.waterPercentage){
			return true;
		}
   	}
	return false;
    }

    @Override
    public int hashCode(){
	int result = super.hashCode();
	Float floatObj = new Float(waterPercentage);
	result = 31*result + floatObj.hashCode();
	return result; 
    } 
}
