package edu.nyu.cs9053.homework4.hierarchy;

public class Stetsonia extends Cactoideae{

    private final float stemWidth;		// Centimeter

    public Stetsonia(String name, String location){
	this(name, location, 30.0f);
    }

    public Stetsonia(String name, String location, float stemWidth){
	super(name, location, LeafType.SPINE);
	this.stemWidth = stemWidth;
    }

    public float getStemWidth(){
	return this.stemWidth;
    }

    @Override
    public boolean equals(Object thatObject){
	boolean equal = super.equals(thatObject);
	if(equal){
		Stetsonia that = (Stetsonia) thatObject;	// in super equals() methods, class comparison already processes
		if(stemWidth == that.stemWidth){
			return true;
		}
	}
	return false;
    }

    @Override
    public int hashCode(){
	int result = super.hashCode();
	Float floatObj = new Float(stemWidth);
	result = 31*result+floatObj.hashCode();
	return result;
    }
}
