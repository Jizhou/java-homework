package edu.nyu.cs9053.homework4.hierarchy;

public class Ferocactus extends Cactoideae{

    private final boolean isMature;

    public Ferocactus(String name, String location){
	this(name, location, false);
    }
    public Ferocactus(String name, String location, boolean isMature){
	super(name, location, LeafType.SPINE);
	this.isMature = isMature;
    }

    public boolean getIsMature(){
	return this.isMature;
    }

    @Override
    public boolean equals(Object thatObject){
	boolean equal = super.equals(thatObject);
	if(equal){
		Ferocactus that = (Ferocactus) thatObject;
		if(isMature == that.isMature){
			return true;
		}
  	}
	return false;
    }

    @Override
    public int hashCode(){
	int result = super.hashCode();
	Boolean booleanObj = new Boolean(isMature);
	result = 31*result + booleanObj.hashCode();
	return result;
    }

}
