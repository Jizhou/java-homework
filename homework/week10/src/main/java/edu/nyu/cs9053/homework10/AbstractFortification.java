package edu.nyu.cs9053.homework10;

/**
 * Created by jizhou on 4/17/16.
 */
public abstract class AbstractFortification implements ConcurrencyFactorProvider{

    private final int concurrencyFactor;

    public AbstractFortification(int concurrencyFactor){
        this.concurrencyFactor = concurrencyFactor;
    }

    @Override
    public int getConcurrencyFactor(){
        return concurrencyFactor;
    }

}
