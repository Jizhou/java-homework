package edu.nyu.cs9053.homework10;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * User: blangel
 * Date: 4/12/16
 * Time: 7:27 PM
 */
public class ModernFortification extends AbstractFortification implements Fortification<ExecutorService> {

    private final ExecutorService attackHandleExecutor;

    public ModernFortification(int concurrencyFactor){
        super(concurrencyFactor);
        attackHandleExecutor = Executors.newScheduledThreadPool(getConcurrencyFactor());
    }

    @Override
    public void handleAttack(AttackHandler handler){

        final AttackHandler finalHandler = handler;

        attackHandleExecutor.submit(new Runnable(){
            @Override
            public void run(){
                finalHandler.soldiersReady();
            }
        });
    }

    @Override
    public void surrender(){
        attackHandleExecutor.shutdownNow();
    }

}