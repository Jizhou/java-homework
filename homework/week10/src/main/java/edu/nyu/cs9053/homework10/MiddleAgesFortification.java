package edu.nyu.cs9053.homework10;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * User: blangel
 * Date: 4/12/16
 * Time: 7:28 PM
 */
public class MiddleAgesFortification extends AbstractFortification implements Fortification<Thread> {

    private final Semaphore attackHandleSemaphore;
    private final AtomicBoolean attackHandleStop;

    public MiddleAgesFortification(int concurrencyFactor){
        super(concurrencyFactor);
        attackHandleSemaphore = new Semaphore(getConcurrencyFactor());
        attackHandleStop = new AtomicBoolean(false);
    }

    @Override
    public void handleAttack(AttackHandler handler){

        final AttackHandler finalHandler = handler;
        if(!attackHandleStop.get()) {

            /**
             * CountDownLatch implementation
             */

            /*
            final CountDownLatch soldierReadyLatch = new CountDownLatch(1);
            Thread soldierDefenseThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    finalHandler.soldiersReady();
                    soldierReadyLatch.countDown();
                }
            });

            soldierDefenseThread.start();
            try {
                soldierReadyLatch.await();
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
                throw new RuntimeException(ie);
            }
            */

            /**
             * Semaphore implementation
             */
            Thread soldierDefenseThread = new Thread(new Runnable(){
                @Override
                public void run(){
                    finalHandler.soldiersReady();
                }
            });

            try{
                attackHandleSemaphore.acquire();
                soldierDefenseThread.start();
                soldierDefenseThread.join();
            }catch(InterruptedException ie){
                Thread.currentThread().interrupt();
                throw new RuntimeException(ie);
            }finally {
                attackHandleSemaphore.release();
            }
        }
    }

    @Override
    public void surrender(){
        attackHandleStop.set(true);
    }

}
