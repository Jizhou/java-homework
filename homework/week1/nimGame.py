import os,sys
import random
import re    
"""NIM game implementation:
   """
heapsNumSet = [3,5,7]
objectNumSet = [9,11,13]
gameHeap = []
humanFirst = False

def gameStart():
	global gameHeap,humanFirst
	gameHeap = [random.choice(objectNumSet) for _ in range(random.choice(heapsNumSet))]
	print "Created "+str(len(gameHeap))+" heaps of sizes "+printHeap()
	if random.randrange(0,2)==0:
		humanFirst = True
		print "Player human goes first"
	else:
		humanFirst = False
		print "Player computer goes first"

def gamePlay():
	global gameHeap
	humanTurn = humanFirst
	cp = computerPlayer(gameHeap)
	inputCheck = True

	while checkEnd(humanTurn)==False:
		if humanTurn == True:
			print "Player human enter the number of objects (Y) to take from what heap (X)- in order: Y X"
			sys.stdout.flush()
			li = raw_input()
			inputCheck = checkInput(li)
			if inputCheck == True:
				li = li.split(" ")
				humanTurn = False
				li[1] = int(li[1])-1
				gameHeap[int(li[1])] = gameHeap[int(li[1])] - int(li[0])
				print printHeap()	

		else:
			humanTurn = True
			li = cp.play()
			print "Player computer took "+str(li[0])+" objects from heap "+str(li[1]+1)
			gameHeap[int(li[1])] = gameHeap[int(li[1])] - int(li[0])
			print printHeap()
			
	

def printHeap():
	"""print out heap status returns string"""
	str1 = ""
	for ele in gameHeap:
		str1 += str(ele)+" "
	return str1

def checkEnd(humanTurn):
	"""check the end of game and output the winner information returns true if the game ends, false otherwise"""
	end = True
	for ele in gameHeap:
		if ele != 0:
			end = False
	if end == True and humanTurn == False:
		"Because in gamePlay() always immediately converse turn after checking turn, so here the checking humanTurn == False means the human just played the turn"
		print "Player human has won"
	elif end == True and humanTurn == True:
		print "Player computer has won"
	return end

def checkInput(li):
	"""check user input value, it first goes to regular expression returns true if it passes input testing, passes false if the input is rejected"""
	regu = "[0-9]+ [0-9]+"
	if re.search(regu,li):
		li = li.split(" ")
		heapNum = int(li[1])-1
		numObj = int(li[0])
		#checkNum = gameHeap[heapNum] - numObj
		if(len(li)!=2 or heapNum < 0 or heapNum > len(gameHeap)):
			print "Player human that is an invalid move, try again"
			return False
		else:
			checkNum = gameHeap[heapNum] - numObj
			if(checkNum >= gameHeap[heapNum] or checkNum < 0):
				print "Player human that is an invalid move, try again"
				return False
			return True
	else:
		print "Player human that is an invalid move, try again"
		return False

# Define computer player class, it takes current heap status as a variable
class computerPlayer(object):
	def __init__(self,currentHeap):
		self.heap = currentHeap

	def play(self):
		"""the computer plays the simplest strategy always make a non-empty heap empty it returns the number of objects removed from heap and heap index"""
		for idx,val in enumerate(self.heap):
			if val != 0:
				return [val,idx]
	


def main():
	gameStart()
	gamePlay()


main()

